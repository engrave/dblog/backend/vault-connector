import { Request, Response } from 'express';
import { handleResponseError, logger } from '../../../submodules/shared-library';
import { param, body } from 'express-validator/check';
import vault from '../../../services/vault/vault.service';

const middleware: any[] =  [
   param("username").isString(),
   param("elevated").isBoolean(),
   body("token").isString(),
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { token } = req.body;
        const { username } = req.params;

        logger.info(`access/store: ${username}`);

        const elevated = (req.params.elevated == 'true');

        await vault.storeAccessToken(username, token, elevated);

        return res.json({
            message: 'OK',
        });
    }, req, res);
}

export default {
    middleware,
    handler
}
