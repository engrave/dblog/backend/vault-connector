import { Request, Response } from 'express';
import { handleResponseError, logger } from '../../../submodules/shared-library';
import { param } from 'express-validator/check';
import vault from '../../../services/vault/vault.service';

const middleware: any[] = [
    param("username").isString()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { username } = req.params;

        logger.info(`refresh/get for ${username}`);

        const token: string = await vault.getRefreshToken(username);

        return res.json({
            message: "OK",
            token: token
        });
    }, req, res);
}

export default {
    middleware,
    handler
}
