import isTokenValid from './actions/isTokenValid';

const vault = {
    isTokenValid
}

export default vault;