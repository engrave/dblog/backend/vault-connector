import unseal from './actions/unseal';
import {logger} from '../../submodules/shared-library';

export async function handleVaultError(handler: Function) {
    try {
        return await handler();
    } catch (e) {
        logger.error('Encountered vault error, unsealing...');
        await unseal();
        logger.info(' * retrying');
        return await handler();
    }
}
