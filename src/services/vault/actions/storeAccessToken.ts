import store from '../store/store';
import { logger } from "../../../submodules/shared-library/utils";
import {handleVaultError} from '../handleVaultError';

export default async (username: string, access_token: string, elevated: boolean) => {
    return await handleVaultError( async () => {
        logger.info(`Store access token for username: ${username}`);

        await store.write(`secret/access/${elevated ? 'dashboard' : 'blog' }/${username}`, { value: access_token})
    });
}
