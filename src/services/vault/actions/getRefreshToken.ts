import store from '../store/store';
import {handleVaultError} from '../handleVaultError';

export default async (username: string) => {
    return await handleVaultError(async () => {
        const { data: {value: token}} = await store.read(`secret/refresh/${username}`)
        return token;
    })
}
