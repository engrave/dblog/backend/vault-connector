import store from '../store/store';

export default async (): Promise<boolean> => {
    const {sealed} = await store.status();
    return sealed;
}
