import store from '../store/store';
import { logger } from "../../../submodules/shared-library/utils";
import {handleVaultError} from '../handleVaultError';

export default async (username: string, refresh_token: string) => {
    return await handleVaultError( async () => {
        logger.info(`Store refresh token for username: ${username}`);
        await store.write(`secret/refresh/${username}`, { value: refresh_token })
    });
}
