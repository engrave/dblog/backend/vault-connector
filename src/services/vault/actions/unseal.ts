import store from '../store/store';
const secrets = require('@cloudreach/docker-secrets');

export default async (): Promise<void> => {
    const keys = JSON.parse(secrets.VAULT_KEYS);
    for (const key of keys) {
        await store.unseal({ secret_shares: 1, key })
    }
}
