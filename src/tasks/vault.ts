import {logger} from '../submodules/shared-library';
import unseal from '../services/vault/actions/unseal';
import getSealedStatus from '../services/vault/actions/getSealedStatus';

export async function checkAndUnsealIfNecessary () {
    try {
        logger.info('Checking if vault is sealed...');
        const isSealed = await getSealedStatus();
        if (isSealed) {
            logger.info(' * Vault is sealed. Unsealing...');
            await unseal();
            logger.info(' * Vault successfully unsealed!');
            return;
        }
    } catch (e) {
        logger.error(e);
    }
}
