import {CronJob} from 'cron';
import {logger} from '../submodules/shared-library';
import {checkAndUnsealIfNecessary} from '../tasks/vault';

function tasks() {
    logger.info("Vault tasks initialized");
    new CronJob('*/10 * * * * *', checkAndUnsealIfNecessary, null, true, 'Europe/Warsaw');
}

export default tasks;
