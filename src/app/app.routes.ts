import healthApi from "../routes/health/health.routes";
import accessApi from "../routes/access/access.routes";
import refreshApi from "../routes/refresh/refresh.routes";
import { endpointLogger } from "../submodules/shared-library/utils/logger";

function routes(app:any) {
    app.use('/health', healthApi);
    app.use('/access', endpointLogger, accessApi);
    app.use('/refresh', endpointLogger, refreshApi);
}

export default routes;

