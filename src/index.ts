import app from './app/app';
import { listenOnPort } from './submodules/shared-library/utils/listenOnPort';

listenOnPort(app, 3000);